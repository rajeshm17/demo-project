import * as React from "react";
import UserListContainer from "../../containers/UserListContainer";

type UserListPageProps = {
  //
};

const UserListPage: React.FC<any> = () => {
  return <UserListContainer/>;
};

export default UserListPage;

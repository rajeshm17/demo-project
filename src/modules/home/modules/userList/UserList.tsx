import React from "react";
import { UserformRepositoryProvider } from "../addUser/repository/AddUser.repository";
import { UsersFacadeProvider } from "./facades/UserList.facade";
import { UsersRepositoryProvider } from "./repository/UserList.repository";
import { UsersServiceProvider } from "./services/UserList.service";
import UserListRoutes from "./UserListRoutes";

const UserList = () => {
  return (
    <UsersRepositoryProvider>
      <UsersServiceProvider>
        <UsersFacadeProvider>
          <UserListRoutes />
        </UsersFacadeProvider>
      </UsersServiceProvider>
    </UsersRepositoryProvider>
  );
};

export default UserList;

import React, { useState } from "react";
import DataGrid, { Editing, Pager, Paging, Sorting } from "devextreme-react/data-grid";
import { columns } from "../../models/constants/userlist.constants";
// const customers=[{CompanyName:"Azilen"},{CompanyName:"Quest Global"}]

type UserListProps = {
  //
};

const UserList: React.FC<any> = (props) => {
  const displayModes = [{ text: 'Display Mode \'full\'', value: 'full' }, { text: 'Display Mode \'compact\'', value: 'compact' }];
  const allowedPageSizes = [5, 10, 'all'];
  const [ModeChange, setModeChange] = useState<string|null>();
  const [PageSizeSelectorChange, setPageSizeSelectorChange] = useState<string|null>();
  const [InfoChange, setInfoChange] = useState<string|null>();
  const [NavButtonsChange, setNavButtonsChange] = useState<string|null>();

  const displayModeChange = (value:any) => {
    setModeChange(value)
  }
  const showPageSizeSelectorChange = (value:any) => {
      setPageSizeSelectorChange(value)
  }
  const showInfoChange = (value:any) => {
    setInfoChange(value)
  }
  const showNavButtonsChange = (value:any) => {
    setNavButtonsChange(value)
      }
        return (
    <>
      <DataGrid
        dataSource={props.users}
        keyExpr="id"
        defaultColumns={columns}
        showBorders={true}
      >
         <Paging defaultPageSize={1} />
         <Sorting mode="multiple" />
         <Pager
            visible={true}
            allowedPageSizes={allowedPageSizes}
            displayMode={ModeChange}
            showPageSizeSelector={PageSizeSelectorChange}
            showInfo={InfoChange}
            showNavigationButtons={NavButtonsChange} 
            />
        <Editing
          mode="form"
          allowUpdating={true}
          allowAdding={true}
          allowDeleting={true}
        />
      </DataGrid>
    </>
  );
};

export default UserList;

import React, { Suspense } from "react";
import { useTranslation } from "react-i18next";
import { Route, Switch, useRouteMatch } from "react-router";
import PageNotFound from "../../../core/presentations/PageNotFound";
const UserListPage = React.lazy(() => import('./pages/UserListPage'));

const UserListRoutes = () => {
  const { t } = useTranslation();
  const match = useRouteMatch();
  return (
    <Switch>
      <Suspense fallback={<div>{t("app.label.loading")}</div>}>
        <Route path={match.path + "/"} exact={true}>
          <UserListPage />
        </Route>
        {/* <Route>
        <PageNotFound />
      </Route> */}
      </Suspense>
    </Switch>
  );
};

export default UserListRoutes;

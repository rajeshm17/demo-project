import * as React from "react";
import { shallow } from "enzyme";
import UserListContainer from "./UserListContainer";

describe("UserListContainer", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<UserListContainer />);
    expect(wrapper).toMatchSnapshot();
  });
});

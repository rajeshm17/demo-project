import * as React from "react";
import { UsersFacadeOperations, useUsersFacade } from "../../facades/UserList.facade";
import UserList from "../../presentations/UserList";

type UserListContainerProps = {
  //
};
type UserProps = { id: string; name: any; email: any };
const UserListContainer: React.FC<any> = () => {
  const [users, setUsers] = React.useState<UserProps[] | []>([]);
  const usersFacade: UsersFacadeOperations | null = useUsersFacade();
  const GetUsersList = async (params?: any) => {
    usersFacade?.getUsersList(params).then((response: any) => {
      console.log("clicked");

      console.log("response from api", response);
      const loadedUsers = [];
      for (const key in response) {
        loadedUsers.push({
          id: key,
          name: response[key].name,
          email: response[key].email,
          age:response[key].Age,
          qualification:response[key].Qualifications,
          birthDate:response[key].BirthDate.substring(0,10),
        });
      }
      console.log("loaded users", loadedUsers);
    

      setUsers(loadedUsers);
    
    });
    
  
    
  };
  React.useEffect(() => {
    GetUsersList();
  }, []);
  return <UserList users={users}/>;
};

export default UserListContainer;

import { createContext, useContext } from "react";
import { UsersServiceOperations, useUsersService } from "../services/UserList.service";


export interface UsersFacadeOperations{
    getUsersList : Function;
  };
  const UsersFacadeContext = createContext< UsersFacadeOperations | null >( null );
  export const UsersFacadeProvider = (props: any) => {
    const UsersService: UsersServiceOperations | null = useUsersService();

    const getUsersList = () => {
        return UsersService?.getUsersList();
      };

      const UsersFacadeOperations : UsersFacadeOperations = {
        getUsersList
      };
    
      return (
        <UsersFacadeContext.Provider value={ UsersFacadeOperations }>
          {props.children}
        </UsersFacadeContext.Provider>
      );
    };
    
    export const useUsersFacade = () => {
      return useContext(UsersFacadeContext);
    };
import * as React from "react";
import AddUserContainer from "../../containers/AddUserContainer";

type AddUserPageProps = {
  //
};

const AddUserPage: React.FC<any> = () => {
  return <AddUserContainer/>;
};

export default AddUserPage;

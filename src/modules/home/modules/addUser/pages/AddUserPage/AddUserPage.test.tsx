import * as React from "react";
import { shallow } from "enzyme";
import AddUserPage from "./AddUserPage";

describe("AddUserPage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<AddUserPage />);
    expect(wrapper).toMatchSnapshot();
  });
});

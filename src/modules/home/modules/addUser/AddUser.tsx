import React from "react";
import AddUserRoutes from "./AddUserRoutes";
import { UserformFacadeProvider } from "./facades/AddUser.facade";
import { UserformRepositoryProvider } from "./repository/AddUser.repository";
import { UserformServiceProvider } from "./services/AddUser.service";

const AddUser = () => {
  return (
    <UserformRepositoryProvider>
      <UserformServiceProvider>
        <UserformFacadeProvider>
          <AddUserRoutes></AddUserRoutes>
        </UserformFacadeProvider>
      </UserformServiceProvider>
    </UserformRepositoryProvider>
  );
};

export default AddUser;

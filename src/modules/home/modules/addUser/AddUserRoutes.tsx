import React, { Suspense } from "react";
import { useTranslation } from "react-i18next";
import { Route, Switch, useRouteMatch } from "react-router";
import PageNotFound from "../../../core/presentations/PageNotFound";
const AddUserPage = React.lazy(() => import('./pages/AddUserPage'));

const AddUserRoutes = () => {
  const match = useRouteMatch();
  const { t } = useTranslation();
console.log("matching route",match);

  return (
    <Switch>
      <Suspense fallback={<div>{t("app.label.loading")}</div>}>
        <Route path={match.path + "/"} exact={true}>
          <AddUserPage />
        </Route>
        {/* <Route>
        <PageNotFound />
      </Route> */}
      </Suspense>
    </Switch>
  );
};

export default AddUserRoutes;

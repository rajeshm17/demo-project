import { createContext, useContext } from "react";
import { UserformServiceOperations, useUserformService } from "../services/AddUser.service";


export interface UserformFacadeOperations{
    createUser : Function;
  };
  const UserformFacadeContext = createContext< UserformFacadeOperations | null >( null );
  export const UserformFacadeProvider = (props: any) => {
    const UserformService: UserformServiceOperations | null = useUserformService();

    const createUser = (body: any) => {
      console.log("userform facade",body);
      
        return UserformService?.createUser(body);
      };

      const UserformFacadeOperations : UserformFacadeOperations = {
        createUser
      };
    
      return (
        <UserformFacadeContext.Provider value={ UserformFacadeOperations }>
          {props.children}
        </UserformFacadeContext.Provider>
      );
    };
    
    export const useUserformFacade = () => {
      return useContext(UserformFacadeContext);
    };
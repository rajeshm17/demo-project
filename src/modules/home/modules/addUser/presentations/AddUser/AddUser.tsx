import * as React from "react";
import "devextreme/dist/css/dx.light.css";
import {
  Form,
  SimpleItem,
  GroupItem,
  TabbedItem,
  Tab,
  ButtonItem,
  TabPanelOptions,
  ButtonOptions,
  RequiredRule,
  EmailRule,
} from "devextreme-react/form";
import { useTranslation } from "react-i18next";
import {
  employee,
  birthDateOptions,
  qualificationOptions
} from "../../models/constants/adduser.constants";
import RCSimpleItem from "../../../../../core/modules/ui/custom/RCSimpleItem";

type AddUserProps = {
  onCreateUser: Function;
};

const AddUser: React.FC<AddUserProps> = (props: AddUserProps) => {
  const { onCreateUser } = props;
  const { t } = useTranslation();
  const handleSubmit = (e: any) => {
    e.preventDefault();
    onCreateUser(employee);
  };

  return (
    <form
      action=""
      onSubmit={handleSubmit}
      className="flex flex-1 flex-col justify-center"
    >
      <Form formData={employee} colCount={2} showColonAfterLabel={true}>
        <GroupItem caption={t("adduser.label.personalInformation")}>
          <TabbedItem>
            <TabPanelOptions height="400" width="300" />
            <Tab title={t("adduser.label.titleInfo")}>
              <SimpleItem dataField={t("adduser.label.name")}>
                <RequiredRule message={t("auth.validation.nameRequired")} />
              </SimpleItem>
              <SimpleItem dataField={t("adduser.label.email")}>
                <RequiredRule message={t("auth.validation.emailRequired")} />
                <EmailRule message={t("auth.validation.incorrectEmail")} />
              </SimpleItem>
              <RCSimpleItem
                dataField="BirthDate"
                editorType="dxDateBox"
                editorOptions={birthDateOptions}
              >
                <RequiredRule
                  message={t("auth.validation.birthdateRequired")}
                />
              </RCSimpleItem>
              <SimpleItem
                dataField="Qualifications"
                editorType="dxSelectBox"
                editorOptions={qualificationOptions}
              >
                <RequiredRule
                  message={t("auth.validation.qualificationRequired")}
                />
              </SimpleItem>
              <SimpleItem dataField="Age" editorType="dxNumberBox">
                <RequiredRule message={t("auth.validation.ageRequired")} />
              </SimpleItem>
              <SimpleItem dataField="T&C" editorType="dxCheckBox">
                <RequiredRule message={t("auth.validation.tncRequired")} />
              </SimpleItem>
            </Tab>
          </TabbedItem>
          <ButtonItem>
            <ButtonOptions
              width={"100%"}
              type={"default"}
              useSubmitBehavior={true}
            >
              {t("adduser.label.submit")}
            </ButtonOptions>
          </ButtonItem>
        </GroupItem>
      </Form>
    </form>
  );
};

export default AddUser;

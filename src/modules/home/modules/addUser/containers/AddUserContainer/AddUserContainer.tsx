import * as React from "react";
import AddUser from "../../presentations/AddUser";
import {
  UserformFacadeOperations,
  useUserformFacade,
} from "../../facades/AddUser.facade";
import { useIsMountedRef } from "../../../../../core/hooks/useIsMounted";
import notify from "devextreme/ui/notify";

type AddUserContainerProps = {
  //
};

const AddUserContainer: React.FC<any> = () => {
  const isMountedRef = useIsMountedRef();
  const userformFacade: UserformFacadeOperations | null = useUserformFacade();
  const handleOnCreateUser = (formData: any) => {
    console.log("formdata received",formData);
    notify("Data Added Successfully");
    userformFacade?.createUser(formData).then((response: any) => {
     
    return response;
    });
  };
  return <AddUser onCreateUser={handleOnCreateUser}/>;
};

export default AddUserContainer;

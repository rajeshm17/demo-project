import React from "react";
import { Redirect, useHistory } from "react-router";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import HomeRoutes from "./HomeRoutes";

const Home = () => {
  const { t } = useTranslation();
  const history =useHistory();
  const logoutHandler=()=>{
    console.log("logout clicked");
    
    localStorage.removeItem('email');
    localStorage.removeItem('isSignedIn');
    history.push('/auth');
  }
  return (
    <>
    <Link to="adduser">{t("home.label.addUser")}</Link>
    <Link to="userlist">{t("home.label.userList")}</Link>
  <HomeRoutes/>
  <button onClick={logoutHandler}>Logout</button>
  </>
  );
};

export default Home;

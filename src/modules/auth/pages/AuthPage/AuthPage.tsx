import * as React from "react";
import { Redirect } from "react-router";
import { useLocation, useHistory } from "react-router-dom";
import { EAComponentProps } from "../../../core/models/interfaces/component.interface";
import LoginContainer from "../../containers/LoginContainer";
import { AuthFacadeOperations, useAuthFacade } from "../../facades/Auth.facade";
import { InitiateLogInDTO, LoginDTO } from "../../models/class/DTOs/Auth";
import { EmailVM, LoginVM } from "../../models/class/VMs/Auth";
import { SCREEN, AUTH_FLOW, AUTH_CONST } from "../../models/enums/auth.enums";

const AuthPage: React.FC<EAComponentProps> = () => {
  console.log("AUTH PAGE");
  
  let history = useHistory();
  let location = useLocation();
  const searchParameter: URLSearchParams = new URLSearchParams(location.search);

  const authFacade: AuthFacadeOperations | null = useAuthFacade();
  const [screen, setScreen] = React.useState(authFacade?.getCurrentAuthScreen());
  const [authFlow, setAuthFlow] = React.useState(authFacade?.getAuthFlow());
  const [authCode, setAuthCode] = React.useState< string | null >("");
  const [userEmail, setUserEmail] = React.useState< string | null >("");
  const [userPassword, setUserPassword] = React.useState("");
  
  React.useEffect(() => {
    let siteName = window.location.host;
    if (!siteName.includes(AUTH_CONST.LOCALHOST)) {
      authFacade?.setTenantId(window.location.host.split(".")[0]);
    }
    if (searchParameter.get(AUTH_CONST.CODE) && searchParameter.get(AUTH_CONST.USERNAME)) {
      setAuthCode(searchParameter.get(AUTH_CONST.CODE));
      setUserEmail(searchParameter.get(AUTH_CONST.USERNAME));
      setScreen(SCREEN.CREATE_PASSWORD);
      setAuthFlow(AUTH_FLOW.SIGNUP);
    }
    // eslint-disable-next-line
  }, []);

  if (window.performance) {
    if (performance.navigation.type === 1) {
      localStorage.removeItem(AUTH_CONST.TAB_CLOSED);
    } else {
      if (localStorage.getItem(AUTH_CONST.TAB_CLOSED) === null) {
        localStorage.setItem(AUTH_CONST.TAB_CLOSED, "yes");
        setScreen(SCREEN.LOGIN);
        setAuthFlow(AUTH_FLOW.LOGIN);
      }
    }
  }

  const switchToResetpasswordScreen = () => {
    setScreen(SCREEN.ENTER_EMAIL);
    setAuthFlow(AUTH_FLOW.RESET_PASSWORD);
  };

  const switchToLoginScreen = () => {
    setScreen(SCREEN.LOGIN);

    setAuthFlow(AUTH_FLOW.LOGIN);
    history.push("/auth");
  };

  const handleLoginSuccess = (response: InitiateLogInDTO, data: LoginVM) => {
    const { session, sub, factor_type } = response;
    const { username, password } = data;
    setUserEmail(username);
    setUserPassword(password);
    
    localStorage.setItem('email',username);
    localStorage.setItem('isSignedIn',"true");
    history.push('/home/adduser')
  
    // authFacade?.setInitiateLoginSession(session, sub, factor_type);

    // setScreen(SCREEN.VERIFY_YOUR_ACCOUNT);
  };

  const handleCodeVerification = (response: LoginDTO) => {
    if (authFlow === AUTH_FLOW.LOGIN) {
      authFacade?.setSession(response.id_token, response.refresh_token);
      authFacade?.setMyself();
      setScreen(SCREEN.LOGIN);
      setAuthFlow(AUTH_FLOW.LOGIN);
      history.push("/home");
    } else if (authFlow === AUTH_FLOW.SIGNUP) {
      setScreen(SCREEN.CREATE_PASSWORD);
    } else if (authFlow === AUTH_FLOW.RESET_PASSWORD) {
      setAuthCode(response.code);
      setScreen(SCREEN.RESET_PASSWORD);
    }
  };

  const handleEmailForResetPasswordSuccess = (body: EmailVM) => {
    setUserEmail(body.email);
    authFacade?.initiateResetPassword(body.email);
    setScreen(SCREEN.VERIFY_YOUR_ACCOUNT);
  };

  const handleResetPasswordSuccessful = () => {
    setScreen(SCREEN.LOGIN);
    setAuthFlow(AUTH_FLOW.LOGIN);
    history.push("/home");
  };

  switch (screen) {
    default:
      return (
        <LoginContainer
          onLoginSuccessful={handleLoginSuccess}
          onClickingResetPassword={switchToResetpasswordScreen}
        />
      );
  }
};

export default AuthPage;

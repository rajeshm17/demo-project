export class LoginDTO {
    "code" : string =  "";
    "expires_in" : number =  86400;
    "id_token" : string =  "";
    "refresh_token" : string =  "";
    "status" : number =  0;
  }
  
  export class InitiateLogInDTO {
    factor_hint : string =  "";
    factor_type : string =  "";
    session : string =  "";
    sub : string =  "";
    status : number = 0;
  }
  
  export class InitiateLoginSessionDTO {
    factor : string =  "";
    session : string =  "";
    sub : string =  "";
  }
  
  export class InitiateLoginSessioDTO {
    "custom:last_login" : string =  "";
    "custom:preferred_factor" : string =  "";
    "custom:tenant_id": string =  "";
    "email" : string =  "";
    "family_name" : string =  "";
    "given_name" : string =  "";
    "phone_number_verified" : string =  ""; 
    "phone_number" : string =  "";
    "picture" : string =  "";
    "role_labels": Array< string > = [];
    "roles": Array< string > = [];
    "sub" : string =  ""; 
  }
  
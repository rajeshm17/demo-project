import React, { createContext, useContext } from "react";
import { EAComponentProps } from "../../core/models/interfaces/component.interface";
import { LoginVM, SignUpVM, VerifyYourAccountCodeVM } from "../models/class/VMs/Auth";
import { AuthServiceOperations, useAuthService } from "../services/Auth.service";

export interface AuthFacadeOperations {
  initiateLogIn : Function;
  logIn : Function;
  resetPassword : Function;
  initiateResetPassword : Function;
  signUp : Function;
  getAuthFlow : Function;
  setAuthFlow : Function;
  getCurrentAuthScreen : Function;
  isLoggedIn : Function;
  getSession : Function;
  setSession : Function;
  getInitiateLoginSession : Function;
  setInitiateLoginSession : Function;
  getTenantId : Function;
  setTenantId : Function;
  setMyself : Function;
  getMyself : Function;
};

const AuthFacadeContext = createContext< AuthFacadeOperations | null >( null );

export const AuthFacadeProvider = (props: EAComponentProps) => {
  const authService: AuthServiceOperations | null = useAuthService();

  const initiateLogIn = (body: LoginVM) => {
      console.log("login facade",body);
      
    return authService?.initiateLogIn(body);
  };

  const logIn = (body: VerifyYourAccountCodeVM) => {
    return authService?.logIn(body);
  };

  const resetPassword = (body: SignUpVM) => {
    return authService?.resetPassword(body);
  };

  const initiateResetPassword = (body: string) => {
    return authService?.initiateResetPassword(body);
  };

  const signUp = (body: SignUpVM) => {
    return authService?.signUp(body);
  };

  const getAuthFlow = () => {
    return authService?.getAuthFlow();
  };

  const setAuthFlow = (flow: string) => {
    return authService?.setAuthFlow(flow);
  };

  const getCurrentAuthScreen = () => {
    return authService?.getCurrentAuthScreen();
  };

  const isLoggedIn = () => {
    let session = authService?.isLoggedIn();
    if (session.idToken) {
      return true;
    }
    return false;
  };

  const getSession = () => {
    return authService?.getSession();
  };

  const setSession = (idToken: string, refreshToken: string) => {
    return authService?.setSession(idToken, refreshToken);
  };

  const getInitiateLoginSession = () => {
    return authService?.getInitiateLoginSession();
  };

  const setInitiateLoginSession = (
    session: string,
    sub: string,
    factor: string
  ) => {
    return authService?.setInitiateLoginSession(session, sub, factor);
  };

  const getTenantId = () => {
    return authService?.getTenantId();
  };

  const setTenantId = (tenantId: string) => {
    return authService?.setTenantId(tenantId);
  };
  
  const setMyself = () => {
    return authService?.setMyself();
  };

  const getMyself = () => {
    return authService?.getMyself();
  };

  const AuthFacadeOperations : AuthFacadeOperations = {
    initiateLogIn,
    logIn,
    resetPassword,
    initiateResetPassword,
    signUp,
    getAuthFlow,
    setAuthFlow,
    getCurrentAuthScreen,
    isLoggedIn,
    getSession,
    setSession,
    getInitiateLoginSession,
    setInitiateLoginSession,
    getTenantId,
    setTenantId,
    setMyself,
    getMyself,
  };

  return (
    <AuthFacadeContext.Provider value={AuthFacadeOperations}>
      {props.children}
    </AuthFacadeContext.Provider>
  );
};

export const useAuthFacade = () => {
  return useContext(AuthFacadeContext);
};

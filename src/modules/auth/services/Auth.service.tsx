import React, { createContext, useContext } from "react";
import { EAComponentProps } from "../../core/models/interfaces/component.interface";
import { LoginVM, SignUpVM, VerifyYourAccountCodeVM } from "../models/class/VMs/Auth";
import { AuthRepoOperations, useAuthRepository } from "../repository/Auth.repository";

export interface AuthServiceOperations {
  initiateLogIn : Function;
  logIn : Function;
  resetPassword : Function;
  initiateResetPassword : Function;
  signUp : Function;
  getAuthFlow : Function;
  setAuthFlow : Function;
  getCurrentAuthScreen : Function;
  isLoggedIn : Function;
  getSession : Function;
  setSession : Function;
  getInitiateLoginSession : Function;
  setInitiateLoginSession : Function;
  getTenantId : Function;
  setTenantId : Function;
  setMyself : Function;
  getMyself : Function;
};
const AuthServiceContext = createContext< AuthServiceOperations | null >( null );

export const AuthServiceProvider = (props: EAComponentProps) => {
  const authRepository: AuthRepoOperations | null = useAuthRepository();

  const initiateLogIn = (body: LoginVM) => {
    return authRepository?.initiateLogIn(body);
  };

  const logIn = (body: VerifyYourAccountCodeVM) => {
    return authRepository?.logIn(body);
  };

  const resetPassword = (body: SignUpVM) => {
    return authRepository?.resetPassword(body);
  };

  const initiateResetPassword = (body: string) => {
    return authRepository?.initiateResetPassword(body);
  };

  const signUp = (body: SignUpVM) => {
    return authRepository?.signUp(body);
  };

  const getAuthFlow = () => {
    return authRepository?.getAuthFlow();
  };

  const setAuthFlow = (flow: string) => {
    return authRepository?.setAuthFlow(flow);
  };

  const getCurrentAuthScreen = () => {
    return authRepository?.getCurrentAuthScreen();
  };

  const isLoggedIn = () => {
    return authRepository?.isLoggedIn();
  };

  const getSession = () => {
    return authRepository?.getSession();
  };

  const setSession = (idToken: string, refreshToken: string) => {
    return authRepository?.setSession(idToken, refreshToken);
  };

  const getInitiateLoginSession = () => {
    return authRepository?.getInitiateLoginSession();
  };

  const setInitiateLoginSession = (
    session: string,
    sub: string,
    factor: string
  ) => {
    return authRepository?.setInitiateLoginSession(session, sub, factor);
  };

  const getTenantId = () => {
    return authRepository?.getTenantId();
  };

  const setTenantId = (tenantId: string) => {
    return authRepository?.setTenantId(tenantId);
  };

  const setMyself = () => {
    return authRepository?.setMyself();
  };

  const getMyself = () => {
    return authRepository?.getMyself();
  };

  const AuthServiceOperations : AuthServiceOperations = {
    initiateLogIn,
    logIn,
    resetPassword,
    initiateResetPassword,
    signUp,
    getAuthFlow,
    setAuthFlow,
    getCurrentAuthScreen,
    isLoggedIn,
    getSession,
    setSession,
    getInitiateLoginSession,
    setInitiateLoginSession,
    getTenantId,
    setTenantId,
    setMyself,
    getMyself,
  };

  return (
    <AuthServiceContext.Provider value={AuthServiceOperations}>
      {props.children}
    </AuthServiceContext.Provider>
  );
};

export const useAuthService = () => {
  return useContext(AuthServiceContext);
};

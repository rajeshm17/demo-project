import { userVM } from "../../models/class/VMs/Auth";
import { AUTH_FLOW, SCREEN, tenantId } from "../../models/enums/auth.enums";
import { InitiateLoginSession, Session } from "../../models/interfaces/auth.interface";

export interface AuthState {
  flow: string;
  screen: string;
  tenantId: string;
  session: Session;
  ActiveUser : userVM;
  initiateLoginSession: InitiateLoginSession;
}

export function createInitialAuthState(): AuthState {
  return {
    flow: AUTH_FLOW.LOGIN,
    screen: SCREEN.LOGIN,
    tenantId: tenantId,
    initiateLoginSession: { session: "", sub: "", factor: "" },
    ActiveUser : new userVM(),
    session: { idToken: "", refreshToken: "" },
  };
}

import React, { createContext, useContext } from "react";
import { EAComponentProps } from "../../core/models/interfaces/component.interface";
import { ApiOperations, useAPIService } from "../../core/services/API.service";
import { InitiateLogInDTO, LoginDTO } from "../models/class/DTOs/Auth";
import { LoginVM, SignUpVM, userVM, VerifyYourAccountCodeVM } from "../models/class/VMs/Auth";
import { API_URLS } from "../models/enums/auth.enums";
import { authQuery, authStore } from "../states/auth";

const { REACT_APP_LOGIN_API_URL,CONSOLE_LOGIN_API_URL } = process.env;

export interface AuthRepoOperations {
  initiateLogIn :Function;
  logIn :Function;
  resetPassword :Function;
  initiateResetPassword :Function;
  signUp :Function;
  getAuthFlow :Function;
  setAuthFlow :Function;
  getCurrentAuthScreen :Function;
  isLoggedIn :Function;
  getSession :Function;
  setSession :Function;
  getInitiateLoginSession :Function;
  setInitiateLoginSession :Function;
  getTenantId :Function;
  setTenantId :Function;
  setMyself :Function;
  getMyself :Function;
};
const AuthRepositoryContext = createContext< AuthRepoOperations | null >( null );

export const AuthRepositoryProvider = (props: EAComponentProps) => {
  const APIService: ApiOperations | null = useAPIService();

  const initiateLogIn = (body: LoginVM) => {
    body.tenantId = authQuery.getValue().tenantId;
    console.log("body inside repository",body);
    
    return APIService?.auth(
        CONSOLE_LOGIN_API_URL,
    body
    ).then((response:any) =>{
        return response;
    //   const initiateLogInDTO = new InitiateLogInDTO();
    //   if(response && response.status === 200){
    //     initiateLogInDTO.factor_hint = response.data.factor_hint;
    //     initiateLogInDTO.factor_type = response.data.factor_type;
    //     initiateLogInDTO.session = response.data.session;
    //     initiateLogInDTO.sub = response.data.sub;
    //     initiateLogInDTO.status = response.status;
    //   }
    //   return initiateLogInDTO;
    });
  };

  const logIn = (body: VerifyYourAccountCodeVM) => {
    body.tenantId = authQuery.getValue().tenantId;
    return APIService?.auth( REACT_APP_LOGIN_API_URL + API_URLS.LOGIN, body).then((response: any) => {
      const loginDTO = new LoginDTO();
      if (response && response.status === 200) {
        loginDTO.expires_in = response.data.expires_in;
        loginDTO.id_token = response.data.id_token;
        loginDTO.refresh_token = response.data.refresh_token;
        loginDTO.status = response.status;
      }
      return loginDTO;
    });;
  };

  const resetPassword = (body: SignUpVM) => {
    body.tenantId = authQuery.getValue().tenantId;
    return APIService?.auth(
      REACT_APP_LOGIN_API_URL + API_URLS.RESET_PASSWORD,
      body
    );
  };

  const initiateResetPassword = (body: string) => {
    let payload = {
      tenantId: authQuery.getValue().tenantId,
      username: body,
    };
    return APIService?.auth(
      REACT_APP_LOGIN_API_URL + API_URLS.INITIATE_RESET_PASSWORD,
      payload
    );
  };

  const signUp = (body: SignUpVM) => {
    body.tenantId = authQuery.getValue().tenantId;
    return APIService?.auth(
      REACT_APP_LOGIN_API_URL + API_URLS.CONSOLE_SIGNUP,
      body
    );
  };

  const getAuthFlow = () => {
    return authQuery.getValue().flow;
  };
  const setAuthFlow = (flow: string) => {
    return authStore.update({ flow: flow });
  };
  const getCurrentAuthScreen = () => {
    return authQuery.getValue().screen;
  };

  const isLoggedIn = () => {
    return authQuery.getValue().session;
  };

  const getSession = () => {
    return authQuery.getValue().session;
  };

  const setSession = (idToken: string, refreshToken: string) => {
    return authStore.update({
      session: { idToken: idToken, refreshToken: refreshToken },
    });
  };

  const getInitiateLoginSession = () => {
    return authQuery.getValue().initiateLoginSession;
  };

  const setInitiateLoginSession = (
    session: string,
    sub: string,
    factor: string
  ) => {
    return authStore.update({
      initiateLoginSession: {
        session: session,
        sub: sub,
        factor: factor,
      },
    });
  };

  const getTenantId = () => {
    return authQuery.getValue().tenantId;
  };

  const setTenantId = (tenantId: string) => {
    return authStore.update({ tenantId: tenantId });
  };

  const setMyself = () => {
    return APIService?.get(REACT_APP_LOGIN_API_URL + API_URLS.GET_MY_SELF).then(
      (response: any) => {
        let user = new userVM();
        user.id = response.data.sub;
        user.key = response.data.sub;
        user.firstName = response.data.given_name;
        user.lastName = response.data.family_name;
        user.phoneNumber = response.data.phone_number.substring(2);
        user.emailAddress = response.data.email;
        user.userImage = response.data.picture;
        user.userType = response.data.roles;
        return authStore.update({
          ActiveUser: user,
        });
      }
    );
  };

  const getMyself = () => {
    return authQuery.getValue().ActiveUser;
  };

  const AuthRepoOperations : AuthRepoOperations = {
    initiateLogIn,
    logIn,
    resetPassword,
    initiateResetPassword,
    signUp,
    getAuthFlow,
    setAuthFlow,
    getCurrentAuthScreen,
    isLoggedIn,
    getSession,
    setSession,
    getInitiateLoginSession,
    setInitiateLoginSession,
    getTenantId,
    setTenantId,
    setMyself,
    getMyself,
  };

  return (
    <AuthRepositoryContext.Provider value={AuthRepoOperations}>
      {props.children}
    </AuthRepositoryContext.Provider>
  );
};

export const useAuthRepository = () => {
  return useContext(AuthRepositoryContext);
};

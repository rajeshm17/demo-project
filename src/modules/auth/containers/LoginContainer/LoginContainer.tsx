import * as React from "react";
import { AuthFacadeOperations, useAuthFacade } from "../../facades/Auth.facade";
import { InitiateLogInDTO } from "../../models/class/DTOs/Auth";
import { LoginVM } from "../../models/class/VMs/Auth";
import Login from "../../presentations/Login";

type LoginContainerProps = {
  onLoginSuccessful: Function;
  onClickingResetPassword: Function;
};

const LoginContainer: React.FC<LoginContainerProps> = (
  props: LoginContainerProps
) => {
  const { onLoginSuccessful, onClickingResetPassword } = props;
  console.log("AUTH LOGINCONTAINER");
  const [userEmail, setUserEmail] = React.useState<string | null>("");
  const [userPassword, setUserPassword] = React.useState("");
  const [error, setError] = React.useState(false);
  const authFacade: AuthFacadeOperations | null = useAuthFacade();

  const handleOnSubmit = (data: LoginVM) => {
    console.log("main data", data);

    const email = data.email;
    const password = data.password;

    setError(false);
    authFacade?.initiateLogIn(data).then((response: InitiateLogInDTO) => {
      console.log("UserEmail", userEmail);

      if (email == "rjs@gmail.com" && password == "Rajesh17#") {
        console.log("condition satisfied");

        onLoginSuccessful(
          {},
          { username: "rjs@gmail.com", password: "Rajesh17#" }
        );
      } else {
        console.log("condition not satisfied");

        setError(true);
      }

      // if (response.status === 200) {
      //   onLoginSuccessful(response,data);
      // } else {
      //   setError(true);
      // }
    });
  };

  const onClickOfResetPassword = () => {
    onClickingResetPassword();
  };

  return (
    <Login
      onSubmit={handleOnSubmit}
      goToResetPassword={onClickOfResetPassword}
      error={error}
    ></Login>
  );
};

export default LoginContainer;

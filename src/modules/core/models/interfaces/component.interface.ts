import { ReactChild } from "react";

export interface EAComponentProps{
    children?: ReactChild;
}
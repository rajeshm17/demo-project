import React, { createContext, useContext } from "react";
import { EAComponentProps } from "../models/interfaces/component.interface";
import { CoreServiceOperations, useCoreService } from "../services/Core.service";

export interface CoreFacadeOperations {
  setUpApplication : Function
};
const CoreFacadeContext = createContext< CoreFacadeOperations | null >( null );

export const CoreFacadeProvider = (props: EAComponentProps) => {
  
  const CoreService: CoreServiceOperations | null = useCoreService();

  const setUpApplication = () => {
    return CoreService?.setUpApplication();
  };

  const CoreFacadeOperations : CoreFacadeOperations = {
    setUpApplication,
  };

  return (
    <CoreFacadeContext.Provider value={CoreFacadeOperations}>
      {props.children}
    </CoreFacadeContext.Provider>
  );
};

export const useCoreFacade = () => {
  return useContext(CoreFacadeContext);
};

import axios from "axios";
import { createContext, useContext } from "react";
import { authQuery, authStore } from "./../../auth/states/auth";
import { API_URLS } from "./../../auth/models/enums/auth.enums";
import {
  APPLICATION_PLATFORM,
  HTTP_STATUS_CODES,
} from "../models/enums/core.enums";
import { EAComponentProps } from "../models/interfaces/component.interface";
const { REACT_APP_LOGIN_API_URL } = process.env;

export interface ApiOperations{
  auth : Function;
  get : Function;
  post : Function;
  put : Function;
  remove : Function;
}
const APIServiceContext = createContext<ApiOperations | null>( null );

export const APIServiceProvider = (props: EAComponentProps) => {
  axios.interceptors.request.use(function (config) {
    const session = authQuery.getValue().session;

    // config.headers["x-client"] = APPLICATION_PLATFORM.MOBILE;
    // if (session.idToken) {
    //   config.headers["x-auth-ident"] = session.idToken;
    // }

    /* TODO replace 19 to 22 with following
    config.headers["x-client"] = APPLICATION_PLATFORM.WEB;
    if (session.idToken) {
      config.headers[
        "Cookies"
      ] = `ident=${session.idToken}; Path=/; Expires=Sat, 30 Jul 2022 12:39:16 GMT;`;
    }
    */

    return config;
  });

  axios.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      const status = error.response ? error.response.status : null;
      const originalRequest = error.config;
      /* TODO api response should have status code, need to be get fixed from backend side */
      if (
        status === HTTP_STATUS_CODES.UNAUTHORIZED &&
        !originalRequest._retry
      ) {
        let url = originalRequest.url;
        if (
          url.includes(API_URLS.INITIATE_CONSOLE_LOGIN) ||
          url.includes(API_URLS.CONSOLE_SIGNUP) ||
          url.includes(API_URLS.INITIATE_RESET_PASSWORD) ||
          url.includes(API_URLS.LOGIN) ||
          url.includes(API_URLS.RESET_PASSWORD) ||
          url.includes(API_URLS.REFRESH_ID_TOKEN)
        ) {
          authStore.update({
            session: { idToken: "", refreshToken: "" },
          });
          authStore.update({
            initiateLoginSession: {
              session: "",
              sub: "",
              factor: "",
            },
          });
          return Promise.reject(error);
        } else {
          originalRequest._retry = true;
          const sessionData = authQuery.getValue().session;
          const initiateLoginSessionData =
            authQuery.getValue().initiateLoginSession;

          const refreshIdTokenRequestPayload = {
            refreshToken: sessionData.refreshToken,
            tenantId: authQuery.getValue().tenantId,
            username: initiateLoginSessionData.sub,
          };
          auth(
            `${REACT_APP_LOGIN_API_URL}${API_URLS.REFRESH_ID_TOKEN}`,
            refreshIdTokenRequestPayload
          ).then(function (response) {
            let idToken = response.data.id_token;
            authStore.update({
              session: {
                idToken: idToken,
                refreshToken: sessionData.refreshToken,
              },
            });
            return axios(originalRequest);
          });
        }
      } else {
        return Promise.reject(error);
      }
    }
  );

  const auth = ( url: string, body: any) => {
    return axios
      .post(url, body)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        return error;
      });
  };

  const get = (url: string) => {
    return axios
      .get(url)
      .then(function (response: any) {
        return response;
      })
      .catch(function (error) {
        return error;
      });
  };

  const post = (url: string, body: any) => {
    return axios
      .post(url, body)
      .then(function (response) {
        return Promise.resolve("Created");
      })
      .catch(function (error) {
        return error;
      });
  };

  const put = (url: string, body: any) => {
    return axios
      .put(url, body)
      .then(function (response) {
        return Promise.resolve("updated");
      })
      .catch(function (error) {
        return error;
      });
  };

  const remove = (url: string, body?: any) => {
    return axios
      .delete(url, body)
      .then(function (response) {
        return Promise.resolve("deleted");
      })
      .catch(function (error) {
        return error;
      });
  };

  const ApiOperations : ApiOperations = {
    auth,
    get,
    post,
    put,
    remove,
  };

  return (
    <APIServiceContext.Provider value={ApiOperations}>
      {props.children}
    </APIServiceContext.Provider>
  );
};

export const useAPIService = () => {
  return useContext(APIServiceContext);
};

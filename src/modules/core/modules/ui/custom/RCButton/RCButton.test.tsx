import * as React from "react";
import { shallow } from "enzyme";
import RCButton from "./RCButton";

describe("RCButton", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<RCButton />);
    expect(wrapper).toMatchSnapshot();
  });
});

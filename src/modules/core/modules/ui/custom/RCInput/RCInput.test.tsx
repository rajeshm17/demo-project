import * as React from "react";
import { shallow } from "enzyme";
import RCInput from "./RCInput";

describe("RCInput", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<RCInput />);
    expect(wrapper).toMatchSnapshot();
  });
});

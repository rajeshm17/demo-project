import React from "react";
import TextBox from "devextreme-react/text-box";
type RCInputProps = {
  label:string,
  placeholder:string,
  mode?: "search" | "text" | "email" | "password" | "tel" | "url" | undefined
};

const RCInput: React.FC<RCInputProps> = (props:RCInputProps) => {
  const {label,placeholder,mode}=props;
  return (
    <div className="dx-field">
      <div className="dx-field-label">{label}</div>
      <div className="dx-field-value">
        <TextBox placeholder={placeholder} mode={mode} showClearButton={true}/>
      </div>
    </div>
  );
};

export default RCInput;

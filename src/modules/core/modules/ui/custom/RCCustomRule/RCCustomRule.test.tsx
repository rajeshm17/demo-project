import * as React from "react";
import { shallow } from "enzyme";
import RCCustomRule from "./RCCustomRule";

describe("RCCustomRule", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<RCCustomRule />);
    expect(wrapper).toMatchSnapshot();
  });
});

import React from "react";
import { Suspense } from "react";
import { useTranslation } from "react-i18next";
import { Switch, Route, Redirect } from "react-router";
import PageNotFound from "./modules/core/presentations/PageNotFound";
import ProtectedRoute from "./modules/core/presentations/ProtectedRoute";
const Auth = React.lazy(() => import("./modules/auth/Auth"));
const Home = React.lazy(() => import("./modules/home/Home"));

export const AppRoutes = () => {
  const { t } = useTranslation();

  return (
    <Switch>
      <Suspense fallback={<div>{t("app.label.loading")}</div>}>
        <Route path={"/auth"}>
          <Auth />
        </Route>
        <ProtectedRoute component={Home} path={"/home"}></ProtectedRoute>
        <Redirect path={"/"} exact={true} to={"/auth"}></Redirect>
        {/* <Route>
        <PageNotFound />
      </Route> */}
      </Suspense>
    </Switch>
  );
};

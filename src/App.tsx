import 'devextreme/dist/css/dx.light.css';
import { AppRoutes } from './AppRoutes';
function App() {
  return (
    <div className="bg-gradient-to-r from-purple-400 to-blue-500 p-60">
      <AppRoutes/>
    </div>
  );
}

export default App;
